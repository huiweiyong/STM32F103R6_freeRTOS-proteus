/**
  **************************************************************************
  * @file:   SSD1306.c
  * @author: 翚伟勇
  * @version:
  * @date:  7,8,2019
  * @brief:
   *************************************************************************
  * @attention:
  **************************************************************************
  */

#include "SSD1306.h"


/* Absolute value */
#define ABS(x)   ((x) > 0 ? (x) : -(x))

uint8_t _i2caddr, _vccstate, x_pos, y_pos, text_size;

/* SSD1306 data buffer */
static uint8_t SSD1306_Buffer[SSD1306_WIDTH * SSD1306_HEIGHT / 8];

/* Private SSD1306 structure */
typedef struct
{
    uint16_t CurrentX;
    uint16_t CurrentY;
    uint8_t Inverted;
    uint8_t Initialized;
} SSD1306_t;
/* Private variable */
static SSD1306_t SSD1306;


static void SSD1306_Command ( unsigned char com );
static void SSD1306_WriteData ( unsigned char dat );

/***************************************************************************
 * @ fn
 * @ brief    写命令
 * @ param
 * @ param
 * @ retval
 **/
static void SSD1306_Command ( unsigned char com )
{
   
#if (SSD1306_SPI==1)
	 int i;
    LCD_CS_RESET;
    LCD_DC_RESET;
    for ( i = 0; i < 8; i++ )
    {
        LCD_SCLK_RESET;
        if ( ( com & 0x80 ) == 0 )
        {
            LCD_SDIN_RESET;
        }
        else
        {
            LCD_SDIN_SET;
        }
        LCD_SCLK_SET;
        com = com << 1;
    }
    LCD_CS_SET;
#elif (SSD1306_IIC==1)
    I2C_Start();
    Send_Byte(0x78);
    I2C_WaitAck();
    Send_Byte(0x00);
    I2C_WaitAck();
    Send_Byte(com);
    I2C_WaitAck();
    I2C_Stop();
#endif
}

/***************************************************************************
 * @ fn
 * @ brief     写数据
 * @ param
 * @ param
 * @ retval
 **/
static void SSD1306_WriteData ( unsigned char dat )
{
   
#if (SSD1306_SPI==1)
	 int i; 
	
    LCD_CS_RESET;
    LCD_DC_SET;
    for ( i = 0; i < 8; ++i )
    {
        LCD_SCLK_RESET;
        if ( ( dat & 0x80 ) == 0 )
        {
            LCD_SDIN_RESET;
        }
        else
        {
            LCD_SDIN_SET;
        }
        LCD_SCLK_SET;
        dat = dat << 1;
    }
    LCD_CS_SET;
#elif(SSD1306_IIC==1)
    I2C_Start();
    Send_Byte(SSD1306_I2C_ADDR);
    I2C_WaitAck();
    Send_Byte(0x40);
    I2C_WaitAck();
    Send_Byte(dat);
    I2C_WaitAck();
    I2C_Stop();
#endif
}

/***************************************************************************
 * @ fn
 * @ brief
 * @ param
 * @ param
 * @ retval
 **/
uint8_t SSD1306_Init ( void )
{
    int i;

    LCD_RES_SET;
    for ( i = 0; i < 20000; i++ );
    for ( i = 0; i < 20000; i++ );
    for ( i = 0; i < 20000; i++ );
    LCD_RES_RESET;
    for ( i = 0; i < 20000; i++ );
    for ( i = 0; i < 20000; i++ );
    for ( i = 0; i < 20000; i++ );
    LCD_RES_SET;
    for ( i = 0; i < 20000; i++ );
    for ( i = 0; i < 20000; i++ );
    for ( i = 0; i < 20000; i++ );

    SSD1306_Command ( SSD1306_DISPLAYOFF );         //关闭显示
    SSD1306_Command ( 0x40 );       //设置屏幕起始行，40h~7Fh
    SSD1306_Command ( SSD1306_SETCONTRAST );
    SSD1306_Command ( 0xCF );

    SSD1306_Command ( SSD1306_MEMORYMODE );         //
    SSD1306_Command ( 0x02 );

    SSD1306_Command ( 0xA4 );

    SSD1306_Command ( 0xA6 );

    SSD1306_Command ( 0xA8 );
    SSD1306_Command ( 0x3F );

    SSD1306_Command ( 0xA0 );   //0xA0或者0xA1
    SSD1306_Command ( 0xC0 );   //设置COM扫描方向，0xC0或者0xC8

    SSD1306_Command ( 0xD3 );
    SSD1306_Command ( 0x00 );

    SSD1306_Command ( SSD1306_SETDISPLAYCLOCKDIV );     //设置时钟分频因子，振荡频率
    SSD1306_Command ( 0x80 );

    SSD1306_Command ( 0xD9 );
    SSD1306_Command ( 0xF1 );

    SSD1306_Command ( 0xDA );
    SSD1306_Command ( 0x12 );

    SSD1306_Command ( 0xDB );
    SSD1306_Command ( 0x30 );

    SSD1306_Command ( SSD1306_CHARGEPUMP );     //电荷泵设置
    SSD1306_Command ( 0x14 );       //bit2, 开启/关闭

    SSD1306_Command ( SSD1306_DISPLAYON );          //打开显示

    return 0;
}

/***************************************************************************
 * @ fn
 * @ brief  设置坐标
 * @ param
 * @ param
 * @ retval
 **/
void ssd1306_Setpos ( unsigned char x, unsigned char y )
{
    SSD1306_Command ( 0xb0 + y );
    SSD1306_Command ( ( ( x & 0xf0 ) >> 4 ) | 0x10 ); //|0x10
    SSD1306_Command ( ( x & 0x0f ) | 0x01 ); //|0x01
}

/***************************************************************************
 * @ fn
 * @ brief     全屏像素反转
 * @ param
 * @ param
 * @ retval
 **/
void SSD1306_ToggleInvert ( void )
{
    uint16_t i;

    /* Toggle invert */
    SSD1306.Inverted = !SSD1306.Inverted;

    /* Do memory toggle */
    for ( i = 0; i < sizeof ( SSD1306_Buffer ); i++ )
    {
        SSD1306_Buffer[i] = ~SSD1306_Buffer[i];
    }
}

/***************************************************************************
 * @ fn
 * @ brief    全屏填充
 * @ param
 * @ param
 * @ retval
 **/
void SSD1306_Fill ( SSD1306_COLOR_t color )
{
    /* Set memory */
    memset ( SSD1306_Buffer, ( color == SSD1306_COLOR_BLACK ) ? 0x00 : 0xFF, sizeof ( SSD1306_Buffer ) );
}

/***************************************************************************
 * @ fn
 * @ brief  画点函数
 * @ param
 * @ param
 * @ retval
 **/
void SSD1306_DrawPixel ( uint16_t x, uint16_t y, SSD1306_COLOR_t color )
{
    if (
        x >= SSD1306_WIDTH ||
        y >= SSD1306_HEIGHT
    )
    {
        /* Error */
        return;
    }

    /* Check if pixels are inverted */
    if ( SSD1306.Inverted )
    {
        color = ( SSD1306_COLOR_t ) !color;
    }

    /* Set color */
    if ( color == SSD1306_COLOR_WHITE )
    {
        SSD1306_Buffer[x + ( y / 8 ) * SSD1306_WIDTH] |= 1 << ( y % 8 );
    }
    else
    {
        SSD1306_Buffer[x + ( y / 8 ) * SSD1306_WIDTH] &= ~ ( 1 << ( y % 8 ) );
    }
}

// move cursor to position (x, y)
void SSD1306_GotoXY ( uint16_t x, uint16_t y )
{
    /* Set write pointers */
    SSD1306.CurrentX = x;
    SSD1306.CurrentY = y;
}

char SSD1306_Putc ( char ch, FontDef_t* Font, SSD1306_COLOR_t color )
{
    uint32_t i, b, j;

    /* Check available space in LCD */
    if (
        SSD1306_WIDTH <= ( SSD1306.CurrentX + Font->FontWidth ) ||
        SSD1306_HEIGHT <= ( SSD1306.CurrentY + Font->FontHeight )
    )
    {
        /* Error */
        return 0;
    }

    /* Go through font */
    for ( i = 0; i < Font->FontHeight; i++ )
    {
        b = Font->data[ ( ch - 32 ) * Font->FontHeight + i];
        for ( j = 0; j < Font->FontWidth; j++ )
        {
            if ( ( b << j ) & 0x8000 )
            {
                SSD1306_DrawPixel ( SSD1306.CurrentX + j, ( SSD1306.CurrentY + i ), ( SSD1306_COLOR_t ) color );
            }
            else
            {
                SSD1306_DrawPixel ( SSD1306.CurrentX + j, ( SSD1306.CurrentY + i ), ( SSD1306_COLOR_t ) !color );
            }
        }
    }

    /* Increase pointer */
    SSD1306.CurrentX += Font->FontWidth;

    /* Return character written */
    return ch;
}



char SSD1306_Putc2 ( char ch, FontDefChar_t* Font, SSD1306_COLOR_t color )
{
    uint32_t i, b, j;

    /* Check available space in LCD */
    if (
        SSD1306_WIDTH <= ( SSD1306.CurrentX + Font->FontWidth ) ||
        SSD1306_HEIGHT <= ( SSD1306.CurrentY + Font->FontHeight )
    )
    {
        /* Error */
        return 0;
    }

    /* Go through font */
    for ( i = 0; i < Font->FontHeight; i++ )
    {
        b = Font->data[ ( ch - 32 ) * Font->FontHeight + i];
        for ( j = 0; j < Font->FontWidth; j++ )
        {
            if ( ( b << j ) & 0x80 )
            {
                SSD1306_DrawPixel ( SSD1306.CurrentX + j, ( SSD1306.CurrentY + i ), ( SSD1306_COLOR_t ) color );
            }
            else
            {
                SSD1306_DrawPixel ( SSD1306.CurrentX + j, ( SSD1306.CurrentY + i ), ( SSD1306_COLOR_t ) !color );
            }
        }
    }

    /* Increase pointer */
    SSD1306.CurrentX += Font->FontWidth;

    /* Return character written */
    return ch;
}


char SSD1306_Puts ( char* str, FontDef_t* Font, SSD1306_COLOR_t color )
{
    /* Write characters */
    while ( *str )
    {
        /* Write character by character */
        if ( SSD1306_Putc ( *str, Font, color ) != *str )
        {
            /* Return error */
            return *str;
        }

        /* Increase string pointer */
        str++;
    }

    /* Everything OK, zero should be returned */
    return *str;
}

/* 取模软件PCtoLCD2002,参数设置逐行式，阴码，顺向（高位在前） */
char SSD1306_Puts2 ( char* str, FontDefChar_t* Font, SSD1306_COLOR_t color )
{
    /* Write characters */
    while ( *str )
    {
        /* Write character by character */
        if ( SSD1306_Putc2 ( *str, Font, color ) != *str )
        {
            /* Return error */
            return *str;
        }

        /* Increase string pointer */
        str++;
    }

    /* Everything OK, zero should be returned */
    return *str;
}


/**************************************************************************/
/*!
   @brief      Draw a RAM-resident 1-bit image at the specified (x,y) position, using the specified foreground color (unset bits are transparent).
    @param    x   Top left corner x coordinate
    @param    y   Top left corner y coordinate
    @param    bitmap  byte array with monochrome bitmap
    @param    w   Width of bitmap in pixels
    @param    h   Height of bitmap in pixels
    @param    color 16-bit 5-6-5 Color to draw with

    取模方式：逐行式，点阵格式，阴码；逆向（低位在前） 软件：PCtoLCD2002
*/
/**************************************************************************/
void SSD1306_putChinese ( uint16_t x, uint16_t y,
                          const uint8_t* bitmap, uint16_t w, uint16_t h, SSD1306_COLOR_t color )
{

   SSD1306_DrawBitmap(x,y,bitmap,w,h,color);
}


void SSD1306_PutNum ( int num, FontDef_t* Font, SSD1306_COLOR_t color )
{
    char ch;
    itoa ( num, &ch );
    SSD1306_Puts ( &ch, Font, color );
}

void SSD1306_DrawLine ( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, SSD1306_COLOR_t c )
{
    int16_t dx, dy, sx, sy, err, e2, i, tmp;

    /* Check for overflow */
    if ( x0 >= SSD1306_WIDTH )
    {
        x0 = SSD1306_WIDTH - 1;
    }
    if ( x1 >= SSD1306_WIDTH )
    {
        x1 = SSD1306_WIDTH - 1;
    }
    if ( y0 >= SSD1306_HEIGHT )
    {
        y0 = SSD1306_HEIGHT - 1;
    }
    if ( y1 >= SSD1306_HEIGHT )
    {
        y1 = SSD1306_HEIGHT - 1;
    }

    dx = ( x0 < x1 ) ? ( x1 - x0 ) : ( x0 - x1 );
    dy = ( y0 < y1 ) ? ( y1 - y0 ) : ( y0 - y1 );
    sx = ( x0 < x1 ) ? 1 : -1;
    sy = ( y0 < y1 ) ? 1 : -1;
    err = ( ( dx > dy ) ? dx : -dy ) / 2;

    if ( dx == 0 )
    {
        if ( y1 < y0 )
        {
            tmp = y1;
            y1 = y0;
            y0 = tmp;
        }

        if ( x1 < x0 )
        {
            tmp = x1;
            x1 = x0;
            x0 = tmp;
        }

        /* Vertical line */
        for ( i = y0; i <= y1; i++ )
        {
            SSD1306_DrawPixel ( x0, i, c );
        }

        /* Return from function */
        return;
    }

    if ( dy == 0 )
    {
        if ( y1 < y0 )
        {
            tmp = y1;
            y1 = y0;
            y0 = tmp;
        }

        if ( x1 < x0 )
        {
            tmp = x1;
            x1 = x0;
            x0 = tmp;
        }

        /* Horizontal line */
        for ( i = x0; i <= x1; i++ )
        {
            SSD1306_DrawPixel ( i, y0, c );
        }

        /* Return from function */
        return;
    }

    while ( 1 )
    {
        SSD1306_DrawPixel ( x0, y0, c );
        if ( x0 == x1 && y0 == y1 )
        {
            break;
        }
        e2 = err;
        if ( e2 > -dx )
        {
            err -= dy;
            x0 += sx;
        }
        if ( e2 < dy )
        {
            err += dx;
            y0 += sy;
        }
    }
}

/***************************************************************************
 * @ fn
 * @ brief     画矩形
 * @ param
 * @ param
 * @ retval
 **/
void SSD1306_DrawRectangle ( uint16_t x, uint16_t y, uint16_t w, uint16_t h, SSD1306_COLOR_t c )
{
    /* Check input parameters */
    if (
        x >= SSD1306_WIDTH ||
        y >= SSD1306_HEIGHT
    )
    {
        /* Return error */
        return;
    }

    /* Check width and height */
    if ( ( x + w ) >= SSD1306_WIDTH )
    {
        w = SSD1306_WIDTH - x;
    }
    if ( ( y + h ) >= SSD1306_HEIGHT )
    {
        h = SSD1306_HEIGHT - y;
    }

    /* Draw 4 lines */
    SSD1306_DrawLine ( x, y, x + w, y, c );      /* Top line */
    SSD1306_DrawLine ( x, y + h, x + w, y + h, c ); /* Bottom line */
    SSD1306_DrawLine ( x, y, x, y + h, c );      /* Left line */
    SSD1306_DrawLine ( x + w, y, x + w, y + h, c ); /* Right line */
}
/***************************************************************************
 * @ fn
 * @ brief  画实心矩形
 * @ param
 * @ param
 * @ retval
 **/
void SSD1306_DrawFilledRectangle ( uint16_t x, uint16_t y, uint16_t w, uint16_t h, SSD1306_COLOR_t c )
{
    uint8_t i;

    /* Check input parameters */
    if (
        x >= SSD1306_WIDTH ||
        y >= SSD1306_HEIGHT
    )
    {
        /* Return error */
        return;
    }

    /* Check width and height */
    if ( ( x + w ) >= SSD1306_WIDTH )
    {
        w = SSD1306_WIDTH - x;
    }
    if ( ( y + h ) >= SSD1306_HEIGHT )
    {
        h = SSD1306_HEIGHT - y;
    }

    /* Draw lines */
    for ( i = 0; i <= h; i++ )
    {
        /* Draw lines */
        SSD1306_DrawLine ( x, y + i, x + w, y + i, c );
    }
}
void SSD1306_DrawTriangle ( uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, SSD1306_COLOR_t color )
{
    /* Draw lines */
    SSD1306_DrawLine ( x1, y1, x2, y2, color );
    SSD1306_DrawLine ( x2, y2, x3, y3, color );
    SSD1306_DrawLine ( x3, y3, x1, y1, color );
}


void SSD1306_DrawFilledTriangle ( uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, SSD1306_COLOR_t color )
{
    int16_t deltax = 0, deltay = 0, x = 0, y = 0, xinc1 = 0, xinc2 = 0,
            yinc1 = 0, yinc2 = 0, den = 0, num = 0, numadd = 0, numpixels = 0,
            curpixel = 0;

    deltax = ABS ( x2 - x1 );
    deltay = ABS ( y2 - y1 );
    x = x1;
    y = y1;

    if ( x2 >= x1 )
    {
        xinc1 = 1;
        xinc2 = 1;
    }
    else
    {
        xinc1 = -1;
        xinc2 = -1;
    }

    if ( y2 >= y1 )
    {
        yinc1 = 1;
        yinc2 = 1;
    }
    else
    {
        yinc1 = -1;
        yinc2 = -1;
    }

    if ( deltax >= deltay )
    {
        xinc1 = 0;
        yinc2 = 0;
        den = deltax;
        num = deltax / 2;
        numadd = deltay;
        numpixels = deltax;
    }
    else
    {
        xinc2 = 0;
        yinc1 = 0;
        den = deltay;
        num = deltay / 2;
        numadd = deltax;
        numpixels = deltay;
    }

    for ( curpixel = 0; curpixel <= numpixels; curpixel++ )
    {
        SSD1306_DrawLine ( x, y, x3, y3, color );

        num += numadd;
        if ( num >= den )
        {
            num -= den;
            x += xinc1;
            y += yinc1;
        }
        x += xinc2;
        y += yinc2;
    }
}

/***************************************************************************
 * @ fn
 * @ brief  画圆
 * @ param
 * @ param
 * @ retval
 **/
void SSD1306_DrawCircle ( int16_t x0, int16_t y0, int16_t r, SSD1306_COLOR_t c )
{
    int16_t f = 1 - r;
    int16_t ddF_x = 1;
    int16_t ddF_y = -2 * r;
    int16_t x = 0;
    int16_t y = r;

    SSD1306_DrawPixel ( x0, y0 + r, c );
    SSD1306_DrawPixel ( x0, y0 - r, c );
    SSD1306_DrawPixel ( x0 + r, y0, c );
    SSD1306_DrawPixel ( x0 - r, y0, c );

    while ( x < y )
    {
        if ( f >= 0 )
        {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;

        SSD1306_DrawPixel ( x0 + x, y0 + y, c );
        SSD1306_DrawPixel ( x0 - x, y0 + y, c );
        SSD1306_DrawPixel ( x0 + x, y0 - y, c );
        SSD1306_DrawPixel ( x0 - x, y0 - y, c );

        SSD1306_DrawPixel ( x0 + y, y0 + x, c );
        SSD1306_DrawPixel ( x0 - y, y0 + x, c );
        SSD1306_DrawPixel ( x0 + y, y0 - x, c );
        SSD1306_DrawPixel ( x0 - y, y0 - x, c );
    }
}

/***************************************************************************
 * @ fn
 * @ brief     画实心圆
 * @ param
 * @ param
 * @ retval
 **/
void SSD1306_DrawFilledCircle ( int16_t x0, int16_t y0, int16_t r, SSD1306_COLOR_t c )
{
    int16_t f = 1 - r;
    int16_t ddF_x = 1;
    int16_t ddF_y = -2 * r;
    int16_t x = 0;
    int16_t y = r;

    SSD1306_DrawPixel ( x0, y0 + r, c );
    SSD1306_DrawPixel ( x0, y0 - r, c );
    SSD1306_DrawPixel ( x0 + r, y0, c );
    SSD1306_DrawPixel ( x0 - r, y0, c );
    SSD1306_DrawLine ( x0 - r, y0, x0 + r, y0, c );

    while ( x < y )
    {
        if ( f >= 0 )
        {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;

        SSD1306_DrawLine ( x0 - x, y0 + y, x0 + x, y0 + y, c );
        SSD1306_DrawLine ( x0 + x, y0 - y, x0 - x, y0 - y, c );

        SSD1306_DrawLine ( x0 + y, y0 + x, x0 - y, y0 + x, c );
        SSD1306_DrawLine ( x0 + y, y0 - x, x0 - y, y0 - x, c );
    }
}


/***************************************************************************
 * @ fn
 * @ brief     区域反转
 * @ param
 * @ param
 * @ retval
 **/
void SSD1306_AreaInvert (uint16_t x, uint16_t y, uint16_t w, uint16_t h )
{
//	    uint16_t i;
//	
//	    /* Toggle invert */
//	    SSD1306.Inverted = !SSD1306.Inverted;
//	
//	    /* Do memory toggle */
//	    for ( i = 0; i < sizeof ( SSD1306_Buffer ); i++ )
//	    {
//	        SSD1306_Buffer[i] = ~SSD1306_Buffer[i];
//	    }


//	uint32_t i, b, j;
	
	   /* Check available space in LCD */
//	   if (
//		   SSD1306_WIDTH <= ( SSD1306.CurrentX + w ) ||
//		   SSD1306_HEIGHT <= ( SSD1306.CurrentY + h )
//	   )
//	   {
//		   /* Error */
//		   return 0;
//	   }
//	
//	   /* Go through font */
//	   for ( i = 0; i < Font->FontHeight; i++ )
//	   {
//		   b = Font->data[ ( ch - 32 ) * Font->FontHeight + i];
//		   for ( j = 0; j < Font->FontWidth; j++ )
//		   {
//			   if ( ( b << j ) & 0x8000 )
//			   {
//				   SSD1306_DrawPixel ( SSD1306.CurrentX + j, ( SSD1306.CurrentY + i ), ( SSD1306_COLOR_t ) color );
//				   
//			   }
//			   else
//			   {
//				   SSD1306_DrawPixel ( SSD1306.CurrentX + j, ( SSD1306.CurrentY + i ), ( SSD1306_COLOR_t ) !color );
//			   }
//		   }
//	   }
//	
//	   /* Increase pointer */
//	   SSD1306.CurrentX += Font->FontWidth;
//	
//	   /* Return character written */
//	   return ch;















	
}


void SSD1306_ON ( void )
{
    SSD1306_Command ( 0x8D );
    SSD1306_Command ( 0x14 );
    SSD1306_Command ( 0xAF );
}
void SSD1306_OFF ( void )
{
    SSD1306_Command ( 0x8D );
    SSD1306_Command ( 0x10 );
    SSD1306_Command ( 0xAE );
}

void Show_mep ( unsigned char x, unsigned char y, unsigned char* Bmp )
{
    unsigned char wm = 0;
    unsigned char* pAdd;
    pAdd = Bmp;
    ssd1306_Setpos ( x, y );
    for ( wm = 0; wm < 15; wm++ )
    {
        SSD1306_WriteData ( *pAdd );
        pAdd += 1;
    }
//  ssd1306_Setpos(x,y + 1);
//  for(wm = 0;wm < 14;wm++)
//  {
//      SSD1306_WriteData(*pAdd);
//      pAdd += 1;
//  }
}
/**
  * @brief  Transmits in master mode an amount of data in blocking mode.
  * @param  hi2c Pointer to a I2C_HandleTypeDef structure that contains
  *                the configuration information for the specified I2C.
  * @param  DevAddress Target device address
  * @param  pData Pointer to data buffer
  * @param  Size Amount of data to be sent
  * @param  Timeout Timeout duration
  * @retval HAL status
  */
//HAL_StatusTypeDef HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
//{
//  if(hi2c->State == HAL_I2C_STATE_READY)
//  {
//    if((pData == NULL) || (Size == 0))
//    {
//      return  HAL_ERROR;
//    }

//    /* Wait until BUSY flag is reset */
//    if(I2C_WaitOnFlagUntilTimeout(hi2c, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG) != HAL_OK)
//    {
//      return HAL_BUSY;
//    }

//    /* Process Locked */
//    __HAL_LOCK(hi2c);

//    /* Disable Pos */
//    CLEAR_BIT(hi2c->Instance->CR1, I2C_CR1_POS);

//    hi2c->State = HAL_I2C_STATE_BUSY_TX;
//    hi2c->Mode = HAL_I2C_MODE_MASTER;
//    hi2c->ErrorCode = HAL_I2C_ERROR_NONE;

//    /* Send Slave Address */
//    if(I2C_MasterRequestWrite(hi2c, DevAddress, Timeout) != HAL_OK)
//    {
//      if(hi2c->ErrorCode == HAL_I2C_ERROR_AF)
//      {
//        /* Process Unlocked */
//        __HAL_UNLOCK(hi2c);
//        return HAL_ERROR;
//      }
//      else
//      {
//        /* Process Unlocked */
//        __HAL_UNLOCK(hi2c);
//        return HAL_TIMEOUT;
//      }
//    }

//    /* Clear ADDR flag */
//    __HAL_I2C_CLEAR_ADDRFLAG(hi2c);

//    while(Size > 0)
//    {
//      /* Wait until TXE flag is set */
//      if(I2C_WaitOnTXEFlagUntilTimeout(hi2c, Timeout) != HAL_OK)
//      {
//        if(hi2c->ErrorCode == HAL_I2C_ERROR_AF)
//        {
//          /* Generate Stop */
//          SET_BIT(hi2c->Instance->CR1,I2C_CR1_STOP);
//          return HAL_ERROR;
//        }
//        else
//        {
//          return HAL_TIMEOUT;
//        }
//      }

//      /* Write data to DR */
//      hi2c->Instance->DR = (*pData++);
//      Size--;

//      if((__HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_BTF) == SET) && (Size != 0))
//      {
//        /* Write data to DR */
//        hi2c->Instance->DR = (*pData++);
//        Size--;
//      }
//    }

//    /* Wait until BTF flag is set */
//    if(I2C_WaitOnBTFFlagUntilTimeout(hi2c, Timeout) != HAL_OK)
//    {
//      if(hi2c->ErrorCode == HAL_I2C_ERROR_AF)
//      {
//        /* Generate Stop */
//        SET_BIT(hi2c->Instance->CR1,I2C_CR1_STOP);
//        return HAL_ERROR;
//      }
//      else
//      {
//        return HAL_TIMEOUT;
//      }
//    }

//    /* Generate Stop */
//    SET_BIT(hi2c->Instance->CR1, I2C_CR1_STOP);

//    hi2c->State = HAL_I2C_STATE_READY;

//    /* Process Unlocked */
//    __HAL_UNLOCK(hi2c);

//    return HAL_OK;
//  }
//  else
//  {
//    return HAL_BUSY;
//  }
//}

//void ssd1306_I2C_WriteMulti(uint8_t address, uint8_t reg, uint8_t* data, uint16_t count) {
//  uint8_t dt[count + 1];
//  dt[0] = reg;
//  uint8_t i;
//  for(i = 1; i <= count; i++)
//      dt[i] = data[i-1];
//  HAL_I2C_Master_Transmit(&hi2c1, address, dt, count, 10);
//}

/***************************************************************************
 * @ fn
 * @ brief
 * @ param
 * @ param
 * @ retval
 **/
void ssd1306_SPI_WriteMulti ( u8* data, u16 count )
{
    u8 n;
    for ( n = 0; n < count; n++ )
    {
        SSD1306_WriteData ( * ( data + n ) );
    }
}
/***************************************************************************
 * @ fn
 * @ brief
 * @ param
 * @ param
 * @ retval
 **/
void SSD1306_UpdateScreen ( void )
{
    uint8_t m;
#if (SSD1306_SPI==1)
    for ( m = 0; m < 8; m++ )
    {
        SSD1306_Command ( 0xB0 + m );
        SSD1306_Command ( 0x00 );
        SSD1306_Command ( 0x10 );

        /* Write multi data */
        ssd1306_SPI_WriteMulti ( &SSD1306_Buffer[SSD1306_WIDTH * m], SSD1306_WIDTH );
    }
#elif (SSD1306_IIC==1)
    for(m = 0; m < 8; m++)
    {
        SSD1306_Command ( 0xB0 + m );
        SSD1306_Command ( 0x00 );
        SSD1306_Command ( 0x10 );

        /* Write multi data */
        ssd1306_I2C_WriteMulti ( SSD1306_I2C_ADDR, 0x40, &SSD1306_Buffer[SSD1306_WIDTH * m], SSD1306_WIDTH );
    }

#endif

}

void drawBitmap ( int16_t x, int16_t y, uint8_t* bitmap, int16_t w, int16_t h, uint16_t color )
{
}

/**************************************************************************/
/*!
   @brief      Draw a RAM-resident 1-bit image at the specified (x,y) position, using the specified foreground color (unset bits are transparent).
    @param    x   Top left corner x coordinate
    @param    y   Top left corner y coordinate
    @param    bitmap  byte array with monochrome bitmap
    @param    w   Width of bitmap in pixels
    @param    h   Height of bitmap in pixels
    @param    color 16-bit 5-6-5 Color to draw with

    取模方式：逐行式，点阵格式，阴码；逆向（低位在前） 软件：PCtoLCD2002
*/
/**************************************************************************/
void SSD1306_DrawBitmap ( uint16_t x, uint16_t y,
                          const uint8_t* bitmap, uint16_t w, uint16_t h, SSD1306_COLOR_t color )
{

    int16_t byteWidth = ( w + 7 ) / 8; // Bitmap scanline pad = whole byte
    uint8_t byte = 0;
    for ( int16_t j = 0; j < h; j++, y++ )
    {
        for ( int16_t i = 0; i < w; i++ )
        {
            if ( i & 7 )
            {
                byte >>= 1;
            }
            else
            {
                byte   = bitmap[j * byteWidth + i / 8];
            }
            if ( byte & 0x01 )
            {
                SSD1306_DrawPixel ( x + i, y, color );
            }
        }
    }
}


/* reverse:  reverse string s in place */
void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = strlen(s) - 1; i < j; i++, j--)
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* itoa:  convert n to characters in s */
void itoa(int n, char s[])
{
    int i, sign;

    if ((sign = n) < 0)  /* record sign */
        n = -n;          /* make n positive */
    i = 0;
    do         /* generate digits in reverse order */
    {
        s[i++] = n % 10 + '0';   /* get next digit */
    }
    while ((n /= 10) > 0);       /* delete it */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}


/***************************************************************************
 * @ fn
 * @ brief
 * @ param
 * @ param
 * @ retval
 **/
void ssd1306_I2C_WriteMulti(uint8_t address, uint8_t reg, uint8_t * data, uint16_t count)
{
    I2C_WriteMulti(address, reg, data, count);
}













