
#include "ST7735.h"




void st7735_spiWrite(uint8_t dt)
{
    // must use HAL_SPI_TransmitReceive - wait for whole byte transmission
    uint8_t rx;
    while(HAL_SPI_TransmitReceive(&hspi1, &dt, &rx, 1, HAL_MAX_DELAY) == HAL_BUSY);
}


void st7735_sendCommand(uint8_t dt)
{
    // must use HAL_SPI_TransmitReceive - wait for whole byte transmission
    uint8_t rx;

    st7735_spiWrite(dt);
}











// for ST7735R
void ST7735_InitR(void)
{
}
// for ST7735B
void ST7735_InitB(void)
{
//      ST7735_Select();
//      ST7735_Reset();
//      ST7735_ExecuteCommandList(init_cmds1);
//      ST7735_ExecuteCommandList(init_cmds2);
//      ST7735_ExecuteCommandList(init_cmds3);
//      ST7735_Unselect();
}


void ST7735_DrawPixel(uint16_t x, uint16_t y, uint16_t color)
{
//      if((x >= ST7735_WIDTH) || (y >= ST7735_HEIGHT))
//          return;
//
//      ST7735_Select();
//
//      ST7735_SetAddressWindow(x, y, x + 1, y + 1);
//      uint8_t data[] = { color >> 8, color & 0xFF };
//      ST7735_WriteData(data, sizeof(data));
//
//      ST7735_Unselect();
}



void ST7735_FillRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color)
{
//      // clipping
//      if((x >= ST7735_WIDTH) || (y >= ST7735_HEIGHT)) return;
//      if((x + w - 1) >= ST7735_WIDTH) w = ST7735_WIDTH - x;
//      if((y + h - 1) >= ST7735_HEIGHT) h = ST7735_HEIGHT - y;
//
//      ST7735_Select();
//      ST7735_SetAddressWindow(x, y, x + w - 1, y + h - 1);
//
//      uint8_t data[] = { color >> 8, color & 0xFF };
//      HAL_GPIO_WritePin(ST7735_DC_GPIO_Port, ST7735_DC_Pin, GPIO_PIN_SET);
//      for(y = h; y > 0; y--)
//      {
//          for(x = w; x > 0; x--)
//          {
//              HAL_SPI_Transmit(&ST7735_SPI_PORT, data, sizeof(data), HAL_MAX_DELAY);
//          }
//      }
//
//      ST7735_Unselect();
}

void ST7735_FillScreen(uint16_t color)
{
//      ST7735_FillRectangle(0, 0, ST7735_WIDTH, ST7735_HEIGHT, color);
}







