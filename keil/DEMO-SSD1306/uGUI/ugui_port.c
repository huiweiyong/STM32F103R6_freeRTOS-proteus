/**
  **************************************************************************
  * @file:   ugui_port.c
  * @author: ��ΰ��  
  * @version:        
  * @date:  11,28,2019
  * @brief:  
   *************************************************************************
  * @attention:
  **************************************************************************
  */

#include "ugui_conf.h"
/***************************************************************************
 * @ fn
 * @ brief
 * @ param
 * @ param
 * @ retval
 **/
void _HW_DrawPoint ( UG_S16 x, UG_S16 y, UG_COLOR c )
{
    static SSD1306_COLOR_t col;

    if(c) col = SSD1306_COLOR_WHITE;
    else col = SSD1306_COLOR_BLACK;
    SSD1306_DrawPixel(x, y, col);
}


/* Hardware accelerator for UG_DrawLine (Platform: STM32F4x9) */
UG_RESULT _HW_DrawLine( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c )
{
    static SSD1306_COLOR_t col;

    if(c) col = SSD1306_COLOR_WHITE;
    else col = SSD1306_COLOR_BLACK;

#if 0
    DMA2D_InitTypeDef DMA2D_InitStruct;

    RCC_AHB1PeriphResetCmd(RCC_AHB1Periph_DMA2D, ENABLE);
    RCC_AHB1PeriphResetCmd(RCC_AHB1Periph_DMA2D, DISABLE);
    DMA2D_InitStruct.DMA2D_Mode = DMA2D_R2M;
    DMA2D_InitStruct.DMA2D_CMode = DMA2D_RGB565;
    /* Convert UG_COLOR to RGB565 */
    DMA2D_InitStruct.DMA2D_OutputBlue = (c >> 3) & 0x1F;
    DMA2D_InitStruct.DMA2D_OutputGreen = (c >> 10) & 0x3F;
    DMA2D_InitStruct.DMA2D_OutputRed = (c >> 19) & 0x1F;
    DMA2D_InitStruct.DMA2D_OutputAlpha = 0x0F;

    /* horizontal line */
    if ( y1 == y2 )
    {
        DMA2D_InitStruct.DMA2D_OutputOffset = 0;
        DMA2D_InitStruct.DMA2D_NumberOfLine = 1;
        DMA2D_InitStruct.DMA2D_PixelPerLine = x2 - x1 + 1;
    }
    /* vertical line */
    else if ( x1 == x2 )
    {
        DMA2D_InitStruct.DMA2D_OutputOffset = LCD_PIXEL_WIDTH - 1;
        DMA2D_InitStruct.DMA2D_NumberOfLine = y2 - y1 + 1;
        DMA2D_InitStruct.DMA2D_PixelPerLine = 1;
    }
    else
    {
        return UG_RESULT_FAIL;
    }

    if ( ltdc_work_layer == LAYER_1 )
    {
        DMA2D_InitStruct.DMA2D_OutputMemoryAdd = SDRAM_BANK_ADDR + LAYER_1_OFFSET + 2 * (LCD_PIXEL_WIDTH * y1 + x1);
    }
    else
    {
        DMA2D_InitStruct.DMA2D_OutputMemoryAdd = SDRAM_BANK_ADDR + LAYER_2_OFFSET + 2 * (LCD_PIXEL_WIDTH * y1 + x1);
    }
    DMA2D_Init(&DMA2D_InitStruct);
    DMA2D_StartTransfer();
    while(DMA2D_GetFlagStatus(DMA2D_FLAG_TC) == RESET) {};
    return UG_RESULT_OK;
#endif
    SSD1306_DrawLine ( x1, y1, x2, y2, col );
	return UG_RESULT_OK;



}

/* Hardware accelerator for UG_FillFrame (Platform: STM32F4x9) */
UG_RESULT _HW_FillFrame( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c )
{
    static SSD1306_COLOR_t col;

    if(c) col = SSD1306_COLOR_WHITE;
    else col = SSD1306_COLOR_BLACK;

#if 0
    DMA2D_InitTypeDef      DMA2D_InitStruct;

    DMA2D_DeInit();
    DMA2D_InitStruct.DMA2D_Mode = DMA2D_R2M;
    DMA2D_InitStruct.DMA2D_CMode = DMA2D_RGB565;
    /* Convert UG_COLOR to RGB565 */
    DMA2D_InitStruct.DMA2D_OutputBlue = (c >> 3) & 0x1F;
    DMA2D_InitStruct.DMA2D_OutputGreen = (c >> 10) & 0x3F;
    DMA2D_InitStruct.DMA2D_OutputRed = (c >> 19) & 0x1F;
    DMA2D_InitStruct.DMA2D_OutputAlpha = 0x0F;
    DMA2D_InitStruct.DMA2D_OutputOffset = (LCD_PIXEL_WIDTH - (x2 - x1 + 1));
    DMA2D_InitStruct.DMA2D_NumberOfLine = y2 - y1 + 1;
    DMA2D_InitStruct.DMA2D_PixelPerLine = x2 - x1 + 1;
    if ( ltdc_work_layer == LAYER_1 )
    {
        DMA2D_InitStruct.DMA2D_OutputMemoryAdd = SDRAM_BANK_ADDR + LAYER_1_OFFSET + 2 * (LCD_PIXEL_WIDTH * y1 + x1);
    }
    else
    {
        DMA2D_InitStruct.DMA2D_OutputMemoryAdd = SDRAM_BANK_ADDR + LAYER_2_OFFSET + 2 * (LCD_PIXEL_WIDTH * y1 + x1);
    }
    DMA2D_Init(&DMA2D_InitStruct);

    DMA2D_StartTransfer();
    while(DMA2D_GetFlagStatus(DMA2D_FLAG_TC) == RESET) {}
    return UG_RESULT_OK;
#endif
SSD1306_DrawFilledRectangle ( x1, y1, (x2-x1), (y2-y1), col );

}



