/**
  **************************************************************************
  * @file:   ugui_conf.h
  * @author: ��ΰ��  
  * @version:        
  * @date:  11,28,2019
  * @brief:  
   *************************************************************************
  * @attention:
  **************************************************************************
  */
	
#ifndef __UGUI_CONF_H__
#define __UGUI_CONF_H__

#include "ugui.h"
#include "ssd1306.h"


extern void _HW_DrawPoint ( UG_S16 x, UG_S16 y, UG_COLOR c );
extern UG_RESULT _HW_DrawLine( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c );
extern UG_RESULT _HW_FillFrame( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c );






#endif






