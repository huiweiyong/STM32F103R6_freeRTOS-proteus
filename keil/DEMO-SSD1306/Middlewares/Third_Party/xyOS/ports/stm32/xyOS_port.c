/**
  **************************************************************************
  * @file:   xyOS_port.c
  * @author: ��ΰ��  
  * @version:        
  * @date:  1,11,2020
  * @brief:  
   *************************************************************************
  * @attention:
  **************************************************************************
  */


#include "xyOS_port.h"


/*********************************************************************
 * GLOBAL VARIABLES
 */

const pTaskEventHandlerFn tasksArr[] =
{
	Hal_ProcessEvent,
	Main_event_loop,
	RF_event_loop,
	DMX512_event_loop,
	LCD_event_loop,
};

const uint8 tasksCnt = sizeof ( tasksArr ) / sizeof ( tasksArr[0] );
uint16* tasksEvents;



/*********************************************************************
 * FUNCTIONS
 *********************************************************************/
/***************************************************************************
 * @ brief     ?�̨�33?��??��
 * @ param
 * @ param
 * @ retval
 **/
void osalInitTasks ( void )
{
	uint8_t taskID = 0;

	tasksEvents = ( uint16* ) osal_mem_alloc ( sizeof ( uint16 ) * tasksCnt );
	osal_memset ( tasksEvents, 0, ( sizeof ( uint16_t ) * tasksCnt ) );

	Hal_Init ( taskID++ );
	Main_Init ( taskID++ );
	RF_Init(taskID++);
	DMX512_Init(taskID++);
	usr_LCDInit ( taskID );
}



