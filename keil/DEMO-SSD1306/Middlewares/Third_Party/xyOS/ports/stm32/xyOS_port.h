/**
  **************************************************************************
  * @file:   xyOS_port.h
  * @author: 翚伟勇  
  * @version:        
  * @date:  1,11,2020
  * @brief:  
   *************************************************************************
  * @attention:
  xyOS移植说明：
  在时钟中断中加入系统心跳函数
  osal_sysTick(); 
  **************************************************************************
  */



#ifndef XYOS_PORT_H
#define XYOS_PORT_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
//#include "hal_mcu.h"
#include "xyOS_Types.h"
#include "hal_drivers.h"
/*********************************************************************
 * MACROS
 */

#define st(x)      do { x } while (__LINE__ == -1)


//#define HAL_ENTER_CRITICAL_SECTION(x)   st( x = 0;  __disable_irq(); )
//#define HAL_EXIT_CRITICAL_SECTION(x)    st( x = x;  __enable_irq(); )
//#define HAL_CRITICAL_STATEMENT(x)  
//	#define HAL_ENTER_CRITICAL_SECTION(x)   st( x = 0;   )
//#define HAL_EXIT_CRITICAL_SECTION(x)    st( x = x;   )
//#define HAL_CRITICAL_STATEMENT(x) 

//#define HAL_ENABLE_INTERRUPTS()      //   st( __enable_irq(); )
//#define HAL_DISABLE_INTERRUPTS()      //  st( __disable_irq(); )
//#define HAL_INTERRUPTS_ARE_ENABLED()  //  (__enable_irq();)

/* Exported macro ------------------------------------------------------------*/
#ifdef  USE_FULL_ASSERT
	
	/**
	  * @brief	The assert_param macro is used for function's parameters check.
	  * @param	expr: If expr is false, it calls assert_failed function which reports 
	  * 		the name of the source file and the source line number of the call 
	  * 		that failed. If expr is true, it returns no value.
	  * @retval None
	  */
  #define assert_param(expr) ((expr) ? (void)0 : assert_failed((uint8_t *)__FILE__, __LINE__))
	/* Exported functions ------------------------------------------------------- */
	  void assert_failed(uint8_t* file, uint32_t line);
#else
  #define assert_param(expr) ((void)0)
#endif /* USE_FULL_ASSERT */

/*********************************************************************
 * CONSTANTS
 */
#define TASK_NO_TASK      0xFF

/*********************************************************************
 * TYPEDEFS
 */


/*
 * Event handler function prototype
 */
typedef unsigned short (*pTaskEventHandlerFn)( unsigned char task_id, unsigned short event );

/*********************************************************************
 * GLOBAL VARIABLES
 */

extern const pTaskEventHandlerFn tasksArr[];
extern const uint8 tasksCnt;
extern uint16 *tasksEvents;

/*********************************************************************
 * FUNCTIONS
 */

/*
 * Call each of the tasks initailization functions.
 */
extern void osalInitTasks( void );
extern void Hal_ProcessPoll (void);

extern void Main_Init( uint8_t task_id );
extern void DMX512_Init(uint8_t task_id);
extern void PWLED_Init( uint8_t task_id );
extern void usr_LCDInit( uint8_t task_id );

extern uint16_t Main_event_loop( uint8_t task_id, uint16_t events );
extern uint16_t DMX512_event_loop( uint8_t task_id, uint16_t events );
extern uint16_t PWLED_event_loop( uint8_t task_id, uint16_t events );
extern uint16_t LCD_event_loop( uint8_t task_id, uint16_t events );


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* OSAL_TASKS_H */

