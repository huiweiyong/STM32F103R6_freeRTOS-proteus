/**
  **************************************************************************
  * @file:   usr_lcd.h
  * @author: 翚伟勇
  * @version:
  * @date:  7,8,2019
  * @brief:
   *************************************************************************
  * @attention:
  适用于液晶型号：XMA12864-03-KSWG30P096-A(驱动IC：SSD1306BZ)

  **************************************************************************
  */


#ifndef __SSD1306_H__
#define __SSD1306_H__

//	#define      SSD1306_CMSIS           0
#define      SSD1306_HAL             1


#define  	SSD1306_IF_IIC       0
#define  	SSD1306_IF_SPI       1

#define     u8      uint8_t
#define     u16    	uint16_t



#if (SSD1306_CMSIS==1)
//#include "stm32f10x.h"
//	#include "fonts.h"
//	#include "hal_i2c.h"
#elif(SSD1306_HAL==1)
#include "stm32f1xx_hal.h"
#include "fonts.h"
#include "stdlib.h"
#endif




#define  SSD1306_I2C_ADDR     0x78


// ONE of the following three lines must be #defined:
#define SSD1306_128_64 ///< DEPRECTAED: old way to specify 128x64 screen
//#define SSD1306_128_32   ///< DEPRECATED: old way to specify 128x32 screen
//#define SSD1306_96_16  ///< DEPRECATED: old way to specify 96x16 screen
// This establishes the screen dimensions in old Adafruit_SSD1306 sketches
// (NEW CODE SHOULD IGNORE THIS, USE THE CONSTRUCTORS THAT ACCEPT WIDTH
// AND HEIGHT ARGUMENTS).

#if (SSD1306_CMSIS==1)
#define   LCD_SDIN_SET         GPIO_SetBits(GPIOB,GPIO_Pin_9) 
#define   LCD_SDIN_RESET       GPIO_ResetBits(GPIOB,GPIO_Pin_9)  

#define   LCD_SCLK_SET         GPIO_SetBits(GPIOB,GPIO_Pin_8) 
#define   LCD_SCLK_RESET       GPIO_ResetBits(GPIOB,GPIO_Pin_8)  

#define   LCD_RES_SET          GPIO_SetBits(GPIOB,GPIO_Pin_7) 
#define   LCD_RES_RESET        GPIO_ResetBits(GPIOB,GPIO_Pin_7)  

#define   LCD_CS_SET           GPIO_SetBits(GPIOB,GPIO_Pin_6) 
#define   LCD_CS_RESET         GPIO_ResetBits(GPIOB,GPIO_Pin_6)  

#define   LCD_DC_SET           GPIO_SetBits(GPIOA,GPIO_Pin_12) 
#define   LCD_DC_RESET         GPIO_ResetBits(GPIOA,GPIO_Pin_12) 


#elif(SSD1306_HAL==1)
#define   LCD_SDIN_SET         HAL_GPIO_WritePin(GPIOC,GPIO_PIN_2,GPIO_PIN_SET) 
#define   LCD_SDIN_RESET       HAL_GPIO_WritePin(GPIOC,GPIO_PIN_2,GPIO_PIN_RESET)  

#define   LCD_SCLK_SET          HAL_GPIO_WritePin(GPIOC,GPIO_PIN_3,GPIO_PIN_SET)
#define   LCD_SCLK_RESET       HAL_GPIO_WritePin(GPIOC,GPIO_PIN_3,GPIO_PIN_RESET)   

#define   LCD_RES_SET          HAL_GPIO_WritePin(GPIOC,GPIO_PIN_1,GPIO_PIN_SET)
#define   LCD_RES_RESET        HAL_GPIO_WritePin(GPIOC,GPIO_PIN_1,GPIO_PIN_RESET)   

#define   LCD_CS_SET            HAL_GPIO_WritePin(GPIOC,GPIO_PIN_0,GPIO_PIN_SET) 
#define   LCD_CS_RESET         HAL_GPIO_WritePin(GPIOC,GPIO_PIN_0,GPIO_PIN_RESET)   

#define   LCD_DC_SET            HAL_GPIO_WritePin(GPIOC,GPIO_PIN_4,GPIO_PIN_SET) 
#define   LCD_DC_RESET         HAL_GPIO_WritePin(GPIOC,GPIO_PIN_4,GPIO_PIN_RESET)  

#endif


/* Exported types ------------------------------------------------------------*/

/**
  * @brief  HAL Status structures definition
  */
#if (SSD1306_CMSIS==1)
  
typedef enum
{
    HAL_OK       = 0x00,
    HAL_ERROR    = 0x01,
    HAL_BUSY     = 0x02,
    HAL_TIMEOUT  = 0x03
} HAL_StatusTypeDef;
#endif

/* SSD1306 settings */
/* SSD1306 width in pixels */
#ifndef SSD1306_WIDTH
#define SSD1306_WIDTH            128
#endif
/* SSD1306 LCD height in pixels */
#ifndef SSD1306_HEIGHT
#define SSD1306_HEIGHT           64
#endif

/**
 * @brief  SSD1306 color enumeration
 */
typedef enum
{
    SSD1306_COLOR_BLACK = 0x00, /*!< Black color, no pixel */
    SSD1306_COLOR_WHITE = 0x01  /*!< Pixel is set. Color depends on LCD */
} SSD1306_COLOR_t;











#define BLACK                          0    ///< Draw 'off' pixels
#define WHITE                          1    ///< Draw 'on' pixels
#define INVERSE                        2    ///< Invert pixels

#define SSD1306_MEMORYMODE          0x20    ///< See datasheet
#define SSD1306_COLUMNADDR          0x21    ///< See datasheet
#define SSD1306_PAGEADDR            0x22    ///< See datasheet
#define SSD1306_SETCONTRAST         0x81    ///< 对比度设置
#define SSD1306_CHARGEPUMP          0x8D    ///< 电荷泵设置
#define SSD1306_SEGREMAP            0xA0    ///< See datasheet
#define SSD1306_DISPLAYALLON_RESUME 0xA4    ///< 全局显示开启;bit0:1,开启;0,关闭;(白屏/黑屏)
#define SSD1306_DISPLAYALLON        0xA5    ///< Not currently used

#define SSD1306_NORMALDISPLAY       0xA6    ///< 正常显示，RAM中的数据1表示点亮像素
#define SSD1306_INVERTDISPLAY       0xA7    ///< 反显模式，RAM中的数据0表示点亮像素
#define SSD1306_SETMULTIPLEX        0xA8    ///< See datasheet
#define SSD1306_DISPLAYOFF          0xAE    ///< 关显示
#define SSD1306_DISPLAYON           0xAF    ///< 开显示
#define SSD1306_COMSCANINC          0xC0    ///< Not currently used
#define SSD1306_COMSCANDEC          0xC8    ///< See datasheet
#define SSD1306_SETDISPLAYOFFSET    0xD3    ///< See datasheet
#define SSD1306_SETDISPLAYCLOCKDIV  0xD5    ///< 设置时钟分频因子，振荡频率
#define SSD1306_SETPRECHARGE        0xD9    ///< See datasheet
#define SSD1306_SETCOMPINS          0xDA    ///< See datasheet
#define SSD1306_SETVCOMDETECT       0xDB    ///< 设置VCOMH 电压倍率

#define SSD1306_SETLOWCOLUMN        0x00    ///< Not currently used
#define SSD1306_SETHIGHCOLUMN       0x10    ///< Not currently used
#define SSD1306_SETSTARTLINE        0x40    ///< See datasheet

#define SSD1306_EXTERNALVCC         0x01    ///< External display voltage source
#define SSD1306_SWITCHCAPVCC        0x02    ///< Gen. display voltage from 3.3V

#define SSD1306_RIGHT_HORIZONTAL_SCROLL              0x26   ///< Init rt scroll
#define SSD1306_LEFT_HORIZONTAL_SCROLL               0x27   ///< Init left scroll
#define SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29 ///< Init diag scroll
#define SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL  0x2A ///< Init diag scroll
#define SSD1306_DEACTIVATE_SCROLL                    0x2E ///< Stop scroll
#define SSD1306_ACTIVATE_SCROLL                      0x2F ///< Start scroll
#define SSD1306_SET_VERTICAL_SCROLL_AREA             0xA3 ///< Set scroll range

// Deprecated size stuff for backwards compatibility with old sketches
#if defined SSD1306_128_64
#define SSD1306_LCDWIDTH  128 ///< DEPRECATED: width w/SSD1306_128_64 defined
#define SSD1306_LCDHEIGHT  64 ///< DEPRECATED: height w/SSD1306_128_64 defined
#endif
#if defined SSD1306_128_32
#define SSD1306_LCDWIDTH  128 ///< DEPRECATED: width w/SSD1306_128_32 defined
#define SSD1306_LCDHEIGHT  32 ///< DEPRECATED: height w/SSD1306_128_32 defined
#endif
#if defined SSD1306_96_16
#define SSD1306_LCDWIDTH   96 ///< DEPRECATED: width w/SSD1306_96_16 defined
#define SSD1306_LCDHEIGHT  16 ///< DEPRECATED: height w/SSD1306_96_16 defined
#endif


/**
 * @brief  Initializes SSD1306 LCD
 * @param  None
 * @retval Initialization status:
 *           - 0: LCD was not detected on I2C port
 *           - > 0: LCD initialized OK and ready to use
 */
uint8_t SSD1306_Init(void);

/**
 * @brief  Updates buffer from internal RAM to LCD
 * @note   This function must be called each time you do some changes to LCD, to update buffer from RAM to LCD
 * @param  None
 * @retval None
 */
void SSD1306_UpdateScreen(void);

/**
 * @brief  Toggles pixels invertion inside internal RAM
 * @note   @ref SSD1306_UpdateScreen() must be called after that in order to see updated LCD screen
 * @param  None
 * @retval None
 */
void SSD1306_ToggleInvert(void);

/**
 * @brief  Fills entire LCD with desired color
 * @note   @ref SSD1306_UpdateScreen() must be called after that in order to see updated LCD screen
 * @param  Color: Color to be used for screen fill. This parameter can be a value of @ref SSD1306_COLOR_t enumeration
 * @retval None
 */
void SSD1306_Fill(SSD1306_COLOR_t Color);

/**
 * @brief  Draws pixel at desired location
 * @note   @ref SSD1306_UpdateScreen() must called after that in order to see updated LCD screen
 * @param  x: X location. This parameter can be a value between 0 and SSD1306_WIDTH - 1
 * @param  y: Y location. This parameter can be a value between 0 and SSD1306_HEIGHT - 1
 * @param  color: Color to be used for screen fill. This parameter can be a value of @ref SSD1306_COLOR_t enumeration
 * @retval None
 */
void SSD1306_DrawPixel(uint16_t x, uint16_t y, SSD1306_COLOR_t color);

/**
 * @brief  Sets cursor pointer to desired location for strings
 * @param  x: X location. This parameter can be a value between 0 and SSD1306_WIDTH - 1
 * @param  y: Y location. This parameter can be a value between 0 and SSD1306_HEIGHT - 1
 * @retval None
 */
void SSD1306_GotoXY(uint16_t x, uint16_t y);

/**
 * @brief  Puts character to internal RAM
 * @note   @ref SSD1306_UpdateScreen() must be called after that in order to see updated LCD screen
 * @param  ch: Character to be written
 * @param  *Font: Pointer to @ref FontDef_t structure with used font
 * @param  color: Color used for drawing. This parameter can be a value of @ref SSD1306_COLOR_t enumeration
 * @retval Character written
 */
char SSD1306_Putc(char ch, FontDef_t* Font, SSD1306_COLOR_t color);
char SSD1306_Putc2 ( char ch, FontDefChar_t* Font, SSD1306_COLOR_t color );


/**
 * @brief  Puts string to internal RAM
 * @note   @ref SSD1306_UpdateScreen() must be called after that in order to see updated LCD screen
 * @param  *str: String to be written
 * @param  *Font: Pointer to @ref FontDef_t structure with used font
 * @param  color: Color used for drawing. This parameter can be a value of @ref SSD1306_COLOR_t enumeration
 * @retval Zero on success or character value when function failed
 */
char SSD1306_Puts(char* str, FontDef_t* Font, SSD1306_COLOR_t color);
char SSD1306_Puts2(char* str, FontDefChar_t* Font, SSD1306_COLOR_t color);


/**
 * @brief  Draws line on LCD
 * @note   @ref SSD1306_UpdateScreen() must be called after that in order to see updated LCD screen
 * @param  x0: Line X start point. Valid input is 0 to SSD1306_WIDTH - 1
 * @param  y0: Line Y start point. Valid input is 0 to SSD1306_HEIGHT - 1
 * @param  x1: Line X end point. Valid input is 0 to SSD1306_WIDTH - 1
 * @param  y1: Line Y end point. Valid input is 0 to SSD1306_HEIGHT - 1
 * @param  c: Color to be used. This parameter can be a value of @ref SSD1306_COLOR_t enumeration
 * @retval None
 */
void SSD1306_DrawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, SSD1306_COLOR_t c);

/**
 * @brief  Draws circle to STM buffer
 * @note   @ref SSD1306_UpdateScreen() must be called after that in order to see updated LCD screen
 * @param  x: X location for center of circle. Valid input is 0 to SSD1306_WIDTH - 1
 * @param  y: Y location for center of circle. Valid input is 0 to SSD1306_HEIGHT - 1
 * @param  r: Circle radius in units of pixels
 * @param  c: Color to be used. This parameter can be a value of @ref SSD1306_COLOR_t enumeration
 * @retval None
 */
void SSD1306_DrawCircle(int16_t x0, int16_t y0, int16_t r, SSD1306_COLOR_t c);

/**
 * @brief  Draws filled circle to STM buffer
 * @note   @ref SSD1306_UpdateScreen() must be called after that in order to see updated LCD screen
 * @param  x: X location for center of circle. Valid input is 0 to SSD1306_WIDTH - 1
 * @param  y: Y location for center of circle. Valid input is 0 to SSD1306_HEIGHT - 1
 * @param  r: Circle radius in units of pixels
 * @param  c: Color to be used. This parameter can be a value of @ref SSD1306_COLOR_t enumeration
 * @retval None
 */
void SSD1306_DrawFilledCircle(int16_t x0, int16_t y0, int16_t r, SSD1306_COLOR_t c);

/**
 * @brief  画图在单片机上
 * @note   @ref SSD1306_UpdateScreen() must be called after that in order to see updated LCD screen
 * @param  x: X location for center of circle. Valid input is 0 to SSD1306_WIDTH - 1
 * @param  y: Y location for center of circle. Valid input is 0 to SSD1306_HEIGHT - 1
 * @param  r: Circle radius in units of pixels
 * @param  c: Color to be used. This parameter can be a value of @ref SSD1306_COLOR_t enumeration
 * @retval None
 */
void SSD1306_DrawBitmap ( uint16_t x, uint16_t y, const uint8_t* bitmap, uint16_t w, uint16_t h, SSD1306_COLOR_t color );


void SSD1306_DrawFilledRectangle ( uint16_t x, uint16_t y, uint16_t w, uint16_t h, SSD1306_COLOR_t c );


#ifndef ssd1306_I2C_TIMEOUT
#define ssd1306_I2C_TIMEOUT                 20000
#endif

/**
 * @brief  Initializes SSD1306 LCD
 * @param  None
 * @retval Initialization status:
 *           - 0: LCD was not detected on I2C port
 *           - > 0: LCD initialized OK and ready to use
 */
void ssd1306_I2C_Init(void);

/**
 * @brief  Writes single byte to slave
 * @param  *I2Cx: I2C used
 * @param  address: 7 bit slave address, left aligned, bits 7:1 are used, LSB bit is not used
 * @param  reg: register to write to
 * @param  data: data to be written
 * @retval None
 */
void ssd1306_I2C_Write(uint8_t address, uint8_t reg, uint8_t data);

/**
 * @brief  Writes multi bytes to slave
 * @param  *I2Cx: I2C used
 * @param  address: 7 bit slave address, left aligned, bits 7:1 are used, LSB bit is not used
 * @param  reg: register to write to
 * @param  *data: pointer to data array to write it to slave
 * @param  count: how many bytes will be written
 * @retval None
 */
void ssd1306_I2C_WriteMulti(uint8_t address, uint8_t reg, uint8_t *data, uint16_t count);

#if(SSD1306_CMSIS==1)
extern void itoa(int n, char s[]);
#endif

extern void SSD1306_PutNum ( int num, FontDef_t* Font, SSD1306_COLOR_t color );

void SSD1306_ON ( void );
void SSD1306_OFF ( void );






#endif





